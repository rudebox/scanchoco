<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="contact padding--bottom">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-12 contact__form">
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

			</div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>
