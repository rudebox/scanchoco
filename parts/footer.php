<footer class="footer black--bg padding--both" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-3 col-sm-offset-1 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
