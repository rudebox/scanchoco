<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//bg
	$img = get_field('page_img') ? : $img = get_field('page_img', 'options') ;
?>

<section class="page__hero" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="arrow"></div>
</section>

<?php 
	//page title visibility
	$show_title = get_field('show_title');
	if ($show_title === true) : 
?>
<section class="page__desc padding--both none--bg">
	<div class="wrap hpad center">
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
	</div>
</section>
<?php endif; ?>
