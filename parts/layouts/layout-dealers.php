<?php
 /**
   * Description: Lionlab dealers repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */
 

 //section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

 if (have_rows('dealer') ) :
?>

<section class="dealers <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row clearfix flex flex--wrap">
		<?php while (have_rows('dealer') ) : the_row(); 
			$img = get_sub_field('logo');
		?>

		<div class="col-sm-4 dealers__item flex flex--hvalign">
			<img class="dealers__img" src="<?php echo esc_url($img['sizes']['medium']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>