<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$add_icon = get_sub_field('add_icon');

if ($add_icon === true) {
	$add_icon = 'icon';
}

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($add_icon); ?>">
	<div class="wrap--fluid hpad">
		<?php if ($title) : ?>
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$icon = get_sub_field('icon');
				$link_type = get_sub_field('type');
				$intern_link = get_sub_field('intern_link');
				$extern_link = get_sub_field('extern_link');
			?>
			
			<?php if ($link_type === 'intern') : ?>
			<a href="<?php echo esc_url($intern_link); ?>" class="col-sm-4 link-boxes__item flex flex--hvalign" style="background-image: url(<?php echo esc_url($icon['url']); ?>);">
			<?php else: ?>
			<a target="_blank" href="<?php echo esc_url($extern_link); ?>" class="col-sm-4 link-boxes__item flex flex--hvalign" style="background-image: url(<?php echo esc_url($icon['url']); ?>);">
			<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>