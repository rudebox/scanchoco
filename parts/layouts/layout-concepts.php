<?php 
/**
* Description: Lionlab concepts repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

$center = get_sub_field('center');

if ($center === true) {
  $center = 'center';
}

if (have_rows('concept_box') ) :
?>

<section class="concepts <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="concepts__header <?php echo esc_attr($center); ?>"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('concept_box') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
			?>
			
			<div class="col-sm-4 concepts__item">	
				<div class="col-sm-2 concepts__icon">
					<?php if ($icon) : ?>
						<?php echo file_get_contents(esc_url($icon['url'])); ?>
					<?php endif; ?>
				</div>

				<div class="col-sm-10 concepts__text">
					<h4 class="concepts__title"><?php echo esc_html($title); ?></h4>
					<?php echo esc_html($text); ?>
				</div>
			</div>
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>