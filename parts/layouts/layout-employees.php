<?php
 /**
   * Description: Lionlab employees repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */
 

 //section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

 if (have_rows('employee') ) :
?>

<section class="employees <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row clearfix flex flex--wrap">
		<?php while (have_rows('employee') ) : the_row(); 
			$img = get_sub_field('img');
			$name = get_sub_field('title');
			$title = get_sub_field('job_title');
			$mail = get_sub_field('mail');
			$phone = get_sub_field('phone');
		?>

		<div class="col-sm-4 employees__item">
			<div class="employees__img">
				<img src="<?php echo esc_url($img['sizes']['employee']); ?>" alt="<?php echo $img['alt']; ?>">
				<div class="arrow"></div>
			</div>
			<div class="employees__content">
				<h5 class="employees__name white"><?php echo esc_html($name); ?></h5>
				<h5 class="employees__title"><?php echo esc_html($title); ?></h5>
				
				<?php if ($mail) : ?>
				<a class="employees__mail white" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
				<?php endif; ?>
				<?php if ($phone) : ?>
				<a class="employees__phone white" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
				<?php endif; ?>
				
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>