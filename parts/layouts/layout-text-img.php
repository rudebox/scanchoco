<?php

// Name of Flexible content clone field

$position = get_sub_field('position');
$img = get_sub_field('img');
$text = get_sub_field('text');
$title = get_sub_field('title');
$title_tag = get_sub_field('title_tag');

$column_class = 'text-image__content entry ' . $position;
$column_class .= $position === 'left' ? ' col-sm-6 col-sm-offset-6' : ' col-sm-6';

//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$heading = get_sub_field('header');
?>

<div class="text-image__wrapper">
	<section class="text-image padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($bg); ?>--bg">
		<div class="wrap hpad">
			<div class="row">

				<div class="<?php echo esc_attr($column_class); ?>">
					
					<?php if ($title_tag === true) : ?>
					<h1 class="text-image__title h2"><?php echo esc_html($title); ?></h1>
					<?php else: ?>
					<h2 class="text-image__title"><?php echo esc_html($title); ?></h2>
					<?php endif; ?>
					<?php echo $text; ?>

				</div>

			</div>
		</div>
	</section>

	<div class="text-image__image <?php echo $position; ?>" style="background-image: url(<?= $img['url']; ?>);"></div>

</div>
