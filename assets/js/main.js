$(document).ready(function() {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //owl slider/carousel
  var owl = $(".slider__track");

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: false,
      dots: true,
      mouseDrag: false,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut'
  });

});
